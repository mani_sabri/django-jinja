#!/usr/bin/env bash

workon djangojinja
PWD=$(pwd)
PATH="$PWD/../node_modules/.bin:$PATH"
export PATH
