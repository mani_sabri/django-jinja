from django.conf import settings
from django_assets import register
from jinja2 import Environment as Jinja2Environment
from unipath import Path
from webassets import Environment as AssetsEnvironment
from webassets.ext.jinja2 import Bundle


if not settings.ASSETS_DEBUG:

    js = Bundle(
        Bundle(
            './js/lib/modernizr-custom.js',
            './js/lib/jquery-2.2.1.min.js',
            './js/lib/angular.min.js',
            './js/lib/bootstrap.min.js',
        ),
        Bundle(
            './js/app.js',
        ),
        filters='jsmin',
        output='./js/packed.js'
    )
    css = Bundle(
        './css/cssreset-min.css',
        './css/bootstrap.min.css',
        './css/bootstrap-theme.min.css',
        './css/site.css',
        filters='cssmin',
        output='./css/packed.css'
    )

else:

    js = Bundle(
        Bundle(
            './js/lib/modernizr-custom.js',
            './js/lib/jquery-2.2.1.min.js',
            './js/lib/angular.min.js',
            './js/lib/bootstrap.min.js',
        ),
        Bundle(
            './js/app.js',
        ),
        filters='jsmin',
        output='./js/packed.js'
    )
    css = Bundle(
        Bundle(
            './css/cssreset-min.css',
            './css/bootstrap.min.css',
            './css/bootstrap-theme.min.css',
        ),
        Bundle(
            './css/site.styl',
            filters='stylus',
            output='./css/site.css'
        ),
        filters='cssmin',
        output='./css/packed.css'
    )


register('js_all', js)
register('css_all', css)


def environment(**options):
    project_dir = Path(__file__).ancestor(2)
    assets_env = AssetsEnvironment(project_dir.child("static"), )
    assets_env.url = settings.ASSETS_URL
    assets_env.debug = settings.ASSETS_DEBUG
    assets_env.register('js_all', js)
    assets_env.register('css_all', css)

    env = Jinja2Environment(**options)
    env.assets_environment = assets_env
    env.globals.update({
        'STATIC_URL': settings.STATIC_URL,
        'MEDIA_URL': settings.MEDIA_URL
    })

    return env
