from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from tinymce.models import HTMLField


class ToDo(models.Model):
    user = models.OneToOneField(User)
    created_at = models.DateTimeField(default=datetime.now)
    item_name = models.CharField(_('name'), max_length=255, null=True, blank=False)
    item_desc = HTMLField(_("description"), null=True, blank=False)
    is_done = models.BooleanField(_('is_done'), default=False)
    is_deleted = models.BooleanField(_('is_deleted'), default=False)
