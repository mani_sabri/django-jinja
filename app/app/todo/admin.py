from django.contrib import admin
from app.todo.models import ToDo


class ToDoAdmin(admin.ModelAdmin):
    list_display = ('user','item_name', 'created_at', 'is_done', 'is_deleted')
    fields = ('item_name', 'is_done', 'is_deleted', 'item_desc')

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


admin.site.register(ToDo, ToDoAdmin)
