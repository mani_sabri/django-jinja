from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from app.views import home
from app.registration.backends.default import urls as registration_urls
from app.todo.views import ToDoViewSet

admin.autodiscover()

router = DefaultRouter()
router.register(r'todos', ToDoViewSet)

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api/api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

urlpatterns += \
    [
        url(r'^$', home),
        url(r'^accounts/', include(registration_urls)),
        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
        url(r'^admin/', include(admin.site.urls)),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
