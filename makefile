s=local

default: run

help:
	cd app && python manage.py help $(s) --settings=app.settings.local

syncdb:
	cd app && python manage.py migrate --run-syncdb --settings=app.settings.$(s)

m:
	cd app && python manage.py migrate --settings=app.settings.$(s)

mm:
	cd app && python manage.py makemigrations --settings=app.settings.$(s)

collectstatic:
	cd app && python manage.py collectstatic -c --settings=app.settings.$(s)

mf:
	cd app && python manage.py migrate $(app) --fake $(ver) --settings=app.settings.$(s)

run:
	cd app && python manage.py runserver --settings=app.settings.$(s)

runext:
	cd app && python manage.py runserver 0.0.0.0:8000 --settings=app.settings.$(s)

shell:
	cd app && python manage.py shell --settings=app.settings.$(s)

cleancache:
	rm -rf app/static/.webassets-cache/
